const cache = require('../cacheFunction.js')
function cb(n) {
    let value
    if (n == 0 || n == 1) {
        value = n
    } else {
        value = cb(n - 1) + cb(n - 2)
    }
    return value
}

const Actual_output = cache(cb)

const Expected_output = 55

if (Actual_output == Expected_output) {
    console.log(Actual_output)
} else {
    console.log('Does not match')
}