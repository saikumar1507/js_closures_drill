const count = require('../counterFactory.js')

let obj = count()

const Actual_output = `{ increment: ${obj.increment()}, decrement: ${obj.decrement()} }`

const Expected_output = `{ increment: ${10}, decrement: ${5} }`

if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log(Actual_output)
} else {
    console.log('Wrong answer')
}
