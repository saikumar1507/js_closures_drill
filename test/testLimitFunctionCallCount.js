const liimitCount = require('../limitFunctionCallCount.js')

function cb(n){
    return n
}

const innerFunc = liimitCount(cb,5)
const Actual_output = innerFunc()

const Expected_output = 5

if(JSON.stringify(Actual_output)==JSON.stringify(Expected_output)){
    console.log(`cb function invoked ${Actual_output} times`)
}else{
    console.log('Not invoked')
}