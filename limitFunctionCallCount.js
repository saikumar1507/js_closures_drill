function limitFunctionCallCount(cb, n) {
    let ans = 0
    function inner() {
        for (let i = 1; i <= n; i++) {
            let val = cb(i)
            ans = val
        }
        return ans
    } 
    return inner
}

module.exports = limitFunctionCallCount