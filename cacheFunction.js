function cacheFunction(cb) {
  let n = 10
  var cache = {}
  function fib(n) {
    if (n in cache) {
      return cache[n]
    } else {
      return cb(n)
    }
  }
  return fib(n)
}
module.exports = cacheFunction