function counterFactory() {
    let counter = 0
    return {
        increment: () => {
            counter += 10
            return counter
        },
        decrement: () => {
            counter -= 5
            return counter
        }
    }
}
module.exports = counterFactory


